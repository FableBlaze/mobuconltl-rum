package rum.algorithms.mobuconltl.test;

import java.io.File;
import java.io.IOException;

import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ltl2automaton.plugins.ltl.SyntaxParserException;

import rum.algorithms.mobuconltl.MoBuConLtLMonitorGlobal;

public class TestGlobal {

	public static void main(String[] args) {

		//String declModelPath = "input/MikeModel_Grade5.decl";
		//String eventLogPath = "input/MikeLog_grade5beginning.xes";
		String declModelPath = "input/sepsis_declareMiner_actSup90.decl";
		String eventLogPath = "input/Sepsis Cases - Event Log.xes";
		
		MoBuConLtLMonitorGlobal monitor = new MoBuConLtLMonitorGlobal();
		
		File declModel = new File(declModelPath);
		try {
			monitor.setModel(declModel);
		} catch (IOException | SyntaxParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		XLog xlog = convertToXlog(eventLogPath);
		for (XTrace xtrace : xlog) {
			for (int i = 0; i < xtrace.size()-1; i++) {
				System.out.println(monitor.processNextEvent(xtrace.get(i), false));
			}
			System.out.println(monitor.processNextEvent(xtrace.get(xtrace.size()-1), true));
			System.out.println();
		}
		
		System.out.println("Done");
		
	}
	
	
	private static XLog convertToXlog(String logPath) {
		XLog xlog = null;
		File logFile = new File(logPath);

		if (logFile.getName().toLowerCase().endsWith(".mxml")){
			XMxmlParser parser = new XMxmlParser();
			if(parser.canParse(logFile)){
				try {
					xlog = parser.parse(logFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (logFile.getName().toLowerCase().endsWith(".xes")){
			XesXmlParser parser = new XesXmlParser();
			if(parser.canParse(logFile)){
				try {
					xlog = parser.parse(logFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return xlog;
	}

}
