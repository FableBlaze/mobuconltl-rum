package rum.algorithms.mobuconltl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.model.XEvent;
import org.processmining.ltl2automaton.plugins.ltl.SyntaxParserException;
import org.processmining.plugins.declareminer.ExecutableAutomaton;

import rum.algorithms.mobuconltl.model.DeclareConstraint;
import rum.algorithms.mobuconltl.utils.AutomatonUtils;
import rum.algorithms.mobuconltl.utils.ModelUtils;

/* 
 * This class creates an individual automaton for each constraint in the model
 * It is slower than the approach with a global automaton, but takes less memory
 * This implementation does not support recovery strategies
 */

public class MoBuConLtlMonitorLocal {
	
	private boolean conflictCheck;

	private List<DeclareConstraint> declareConstrains;
	private Map<String, String> activityToEncoding;
	private Map<DeclareConstraint, ExecutableAutomaton> constraintAutomatonMap = new HashMap<DeclareConstraint, ExecutableAutomaton>();
	private List<String> ltlFormulas = new ArrayList<String>();
	private ExecutableAutomaton globalAutomaton;
	private boolean permVioloccurred;

	public MoBuConLtlMonitorLocal(boolean conflictCheck) {
		this.conflictCheck = conflictCheck;
	}


	//Loads the input model and creates the individual automata for monitoring
	public void setModel(File declModel) throws IOException, SyntaxParserException {

		//Loading constraints from the model
		declareConstrains = ModelUtils.readConstraints(declModel);
		System.out.println("Constraints: " + declareConstrains);


		//Creating activity name encodings to avoid issues with activity names containing dashes etc.
		activityToEncoding = ModelUtils.encodeActivities(declareConstrains);
		System.out.println("Activity encodings: " + activityToEncoding);


		//Creating automata for monitoring
		System.out.println("Encoded LTL formulas:");
		for (DeclareConstraint declareConstraint : declareConstrains) {
			String ltlFormula = AutomatonUtils.getGenericLtlFormula(declareConstraint.getTemplate());

			//Replacing activity placeholders in the generic formula with activity encodings based on the model
			ltlFormula = ltlFormula.replace("\"A\"", activityToEncoding.get(declareConstraint.getActivationActivity()));
			if (declareConstraint.getTemplate().getIsBinary()) {
				ltlFormula = ltlFormula.replace("\"B\"", activityToEncoding.get(declareConstraint.getTargetActivity()));
			}
			System.out.println("\t" + ltlFormula);

			//Creating the constraint automaton, initialising it, and placing it in the automata map
			ExecutableAutomaton constraintAutomaton = new ExecutableAutomaton(AutomatonUtils.createAutomatonForLtlFormula(ltlFormula));
			constraintAutomaton.ini();
			constraintAutomatonMap.put(declareConstraint, constraintAutomaton);
			
			if (conflictCheck) {
				ltlFormulas.add(ltlFormula);
			}
		}
		
		//Creating global automata for conflict check
		if (conflictCheck) {
			String globalLtlFormula = "(" + String.join(") && (", ltlFormulas) + ")";
			globalAutomaton = new ExecutableAutomaton(AutomatonUtils.createAutomatonForLtlFormula(globalLtlFormula));
			globalAutomaton.ini();
		}
		
		System.out.println("Model processing done!");
	}

	//Processes the next event in a trace, assumes that processing occurs one trace at a time
	//isTraceEnd=True returns permanent states instead of temporary states and resets automata for monitoring the next trace
	public Map<String, String> processNextEvent(XEvent xevent, boolean isTraceEnd) {
		Map<String, String> constraintStates = new HashMap<String, String>();

		String eventName = XConceptExtension.instance().extractName(xevent);
		String encodedEventName = activityToEncoding.getOrDefault(eventName, "actx");

		for (DeclareConstraint declareConstraint : declareConstrains) {
			ExecutableAutomaton constraintAutomaton = constraintAutomatonMap.get(declareConstraint);
			constraintAutomaton.next(encodedEventName);

			MonitoringState monitoringState = AutomatonUtils.getMonitoringState(constraintAutomaton.currentState());
			constraintStates.put(declareConstraint.getConstraintString(), monitoringState.getMobuconltlName());
		}
		
		//Using the global automaton to detect conflicting states
		if (conflictCheck) {
			if (isTraceEnd) {
				globalAutomaton.ini();
				permVioloccurred = false;
			} else {
				if (!permVioloccurred) {
					for (String monitoringStateString : constraintStates.values()) {
						if ("viol".equals(monitoringStateString)) {
							permVioloccurred = true;
						}
					}
				}
				
				if (!permVioloccurred) {
					globalAutomaton.next(encodedEventName);
					if (!globalAutomaton.currentState().acceptingReachable()) {
						for (String constraint : constraintStates.keySet()) {
							//Marking all constraints (except sat) as conflicting
							if (!constraintStates.get(constraint).equals("sat")) {
								constraintStates.put(constraint, "conflict");
							}
						}
					}
				}
			}
		}

		//All monitoring states become permanent at the end of the trace
		if (isTraceEnd) {
			for (String constraint : constraintStates.keySet()) {
				if (constraintStates.get(constraint).equals("poss.sat")) {
					constraintStates.put(constraint, "sat");
				}
				if (constraintStates.get(constraint).equals("poss.viol")) {
					constraintStates.put(constraint, "viol");
				}
			}

			for (ExecutableAutomaton constraintAutomaton : constraintAutomatonMap.values()) {
				constraintAutomaton.ini();
			}
		}

		return constraintStates;
	}

}
