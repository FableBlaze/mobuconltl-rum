
Re-implementation of the MoBuCon monitoring apporach originally presented in [1]. This implementation does not include for example operational support, process health measure, the original visualisation, etc., and is designed to be used as a dependency in RuM [2], however it should be usable also in other Java applications. 


[1] Maggi F.M., Montali M., Westergaard M., van der Aalst W.M.P. (2011) Monitoring Business Constraints with Linear Temporal Logic: An Approach Based on Colored Automata. In: Rinderle-Ma S., Toumani F., Wolf K. (eds) Business Process Management. BPM 2011. Lecture Notes in Computer Science, vol 6896. Springer, Berlin, Heidelberg. https://doi.org/10.1007/978-3-642-23059-2_13

[2] A. Alman, C. D. Ciccio, D. Haas, F. M. Maggi and A. Nolte, "Rule Mining with RuM," 2020 2nd International Conference on Process Mining (ICPM), 2020, pp. 121-128, doi: 10.1109/ICPM49681.2020.00027.